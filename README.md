### What does it do? ###
Defines markers for classifying concrete entities in your project according to different design practices, like :

 - domain driven design : defines a Value object, an Entity object, etc.
 - hexagonal architecture (aka ports and adapters) : defines a Domain layer, a Presentation layer, etc.
 - design patterns 

### What is this project for? ###
 - help express the usage of a well know concept, in a certain area of design, in a specific part of your project
 - (potentially) collect information about your project structure and expose it through a REST API or through visual representations

### Who do I talk to? ###
alexandrurepede@gmail.com