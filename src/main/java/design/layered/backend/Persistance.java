package design.layered.backend;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Backend
public @interface Persistance {
}
