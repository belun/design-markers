package design.layered.backend;

import design.layered.frontend.Frontend;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Frontend
public @interface Backend {
}
