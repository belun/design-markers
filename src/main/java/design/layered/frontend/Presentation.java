package design.layered.frontend;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Frontend
public @interface Presentation {
}
